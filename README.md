# Django Angular Test App

This is a sample web application with a [Django][django-website] backend providing the APIs,
and an [Angular][angular-website] frontend providing the UI.

[django-website]: https://www.djangoproject.com/
[angular-website]: https://angular.io/


## Meta Goals

1. To serve as a personal reference on basic usage of Django, and on the
following areas:
    - Idiomatic usage of [Django REST framework][drf-website]
    - Authentication
    - Object-level authorization
    - Customizing Django administration
    - Automated testing
1. To learn [TypeScript][typescript-website] in the context of Angular.
1. To serve as personal reference on basic usage of Angular, and on the
following areas:
    - Styling of UI using [Angular Material][angular-material-website]
    - Using both [Reactive Forms][reactive-forms-link] and
    [Template-driven Forms][template-driven-forms-link]
    - Automated testing
1. CI/CD in the context of two microservices (the backend APIs and frontend UI).

[drf-website]: https://www.django-rest-framework.org/
[typescript-website]: https://www.typescriptlang.org/
[angular-material-website]: https://material.angular.io/
[reactive-forms-link]: https://angular.io/guide/reactive-forms
[template-driven-forms-link]: https://angular.io/guide/forms


## Features

The app allow the staff client account managers / business analysts of a
ficticious IT company to record feature requests from clients.

Notable details include:
1. Each staff will correspond to a `User` in the app.
1. Each `User` can be assigned to any number of `Clients`.
1. Each `User` may create any number of `FeatureRequests` for each `Client`:
    1. Each `FeatureRequest` may be assigned a priority.
    1. No `FeatureRequests` of a particular `Client` can have the same priority.


## Testing Philosophy

### Views

In order to achieve fast views tests, the view methods/classes will be tested
separately from the routing. What this means is that the tests will generally take the following form:
1. `django.test.RequestFactory` will be used to create a request object.
1. The request object will be passed to the view methods/classes directly
    - _Note: Since the request object is passed directly to the view
      methods/classes directly, the URL specified when creating the request
      object should generally not matter._
1. The response object returned from the view methods/classes will be used for
   the assertions.

Routing should be tested separately.

## Choice of Authentication

The choice of authentication (and authorization) is of some interest
worth noting because the Django backend is meant solely to provide API
to support the Angular frontend, which leads to the following design
considerations:
1. Authentication request would originate from a different domain
   (i.e., where the Angular frontend is hosted) from the Django
   backend.
1. Any login form(s) should be on the Angular frontend, and users
   should not be required to authenticate directly on the domain
   hosting the Django backend.
1. There should not be any two-step authorization process as the
   Django backend and Angular frontend are meant to be one application

The original choices of authentication providers are as follows:
1. [Native authentication provided by
   Django](https://docs.djangoproject.com/en/2.2/topics/auth/)
1. [Django OAuth
   Toolkit](https://django-oauth-toolkit.readthedocs.io/en/latest/)
1. [django-rest-auth](https://github.com/Tivix/django-rest-auth)
1. [Simple
   JWT](https://github.com/davesque/django-rest-framework-simplejwt)

In the end, Simple JWT is chosen. Let us see why the first 3 options
are eliminated.

### Native authenication

Django provides native authentication based on the ~User~ model,
complete with views, routes and forms. However, this native
authentication assumes that authentication flow happens on the Django
application itself.

It is possible to customize the native authentication to support
authentication from the Angular frontend hosted on a different
domain, but this approach is not taken for 2 main reasons:
1. Avoid re-inventing the wheel.
1. Security concerns.

Customizing the native authentication will likely involve handling the
CSRF token on both the backend and frontend with custom code
(re-inventing the wheel), or disabling the CSRF checks while still
using session-based authentication (introducing security concerns).

### Django OAuth Toolkit

Django OAuth Toolkit is a mature and full-feature plugin which adds
OAuth2 capabilities to the Django backend.

However, this is not chosen for the following reasons:
1. In order to avoid having a two-step authentication flow (i.e.,
   first, user visit the Angular frontend which redirects to a
   specific URL on the Django backend; second, user logs in to the
   Django backend and grants authorization) the OAuth [Grant
   Types][oauth-grant-types] has to be
   "[Password][oauth-grant-password]". However, for Django OAuth
   Toolkit, this grant type requires the client application to submit
   in the POST request both the client ID and the client secret. Since
   our Angular frontend has no way to keep the client secret a secret,
   this approach is out. (Note: Based on this okta article on [What is
   the OAuth 2.0 Password Grant Type?][okta-article-password], the
   client secret is optional.
1. If we use either the "[Authorization Code][oauth-grant-code]" or
   "[Implicit][oauth-grant-implicit" grant type, user would have to
   log in on the Django backend using the two-step flow, which is
   unnatural since the Django backend and Angular frontend is a single
   applicaiton.

[oauth-grant-types]: https://oauth.net/2/grant-types/
[oauth-grant-password]: https://oauth.net/2/grant-types/password/
[okta-article-password]: https://developer.okta.com/blog/2018/06/29/what-is-the-oauth2-password-grant
[oauth-grant-code]: https://oauth.net/2/grant-types/authorization-code/
[oauth-grant-implicit]: https://oauth.net/2/grant-types/implicit/

### django-rest-auth

django-rest-auth is a plugin that provides various REST endpoints for
authentication related workflows (e.g., registration, password reset,
etc.). As such it fits rather nicely to the design of Django Rest
Framework. However, the default authentication method of
django-rest-auth uses Django's own token-based authentication, which
is very barebones (e.g., there is no concept of token
expiration). django-rest-auth does support JWT, however, this is
achieved through a dependency to [REST framework JWT
Auth][rest-framework-jwt-auth], which is no longer maintained.

[rest-framework-jwt-auth]: https://github.com/jpadilla/django-rest-framework-jwt

### Simple JWT

Due to the above considerations, Simple JWT becomes the natural choice
for the authentication provider.

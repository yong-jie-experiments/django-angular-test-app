"""This file tests that the interface with third-party plugin Simple JWT (at
https://github.com/davesque/django-rest-framework-simplejwt) works as
expected.

The tests contained in this file uses APIClient instead of APIRequestFactory,
because APIClient will trigger the middlewares (including Simple JWT) whereas
APIRequestFactory (which is used in the views tests) bypasses middleware and
call the view methods directly.
"""

import json

from django.test import TestCase
from rest_framework.test import APIClient

from clients.models import Client
from clients.serializers import ClientSerializer
from users.models import User


class JwtObtainRefreshTokensTestCase(TestCase):

    def test_obtain_jwt_pair__correct_credentials(self):

        # Arrange
        user = User(username='User', email='user@example.com')
        user.set_password('password')
        user.save()

        jwt_keys_expected = sorted(['refresh', 'access'])

        # Act
        res = APIClient().post('/api/token/',
                               {'username': 'User', 'password': 'password'},
                               format='json')

        # Assert
        self.assertEqual(res.status_code, 200)
        jwt_actual = json.loads(res.content)
        jwt_keys_actual = sorted(list(jwt_actual.keys()))
        self.assertListEqual(jwt_keys_actual, jwt_keys_expected)

    def test_obtain_jwt_pair__missing_credentials(self):

        # Arrange
        user = User(username='User', email='user@example.com')
        user.set_password('password')
        user.save()

        res_payload_expected = {'username': ['This field may not be blank.'],
                                'password': ['This field is required.']}

        # Act
        res = APIClient().post('/api/token/', {'username': ''}, format='json')

        # Assert
        self.assertEqual(res.status_code, 400)
        res_payload_actual = json.loads(res.content)
        self.assertDictEqual(res_payload_actual, res_payload_expected)

    def test_obtain_jwt_pair__wrong_credentials(self):

        # Arrange
        user = User(username='User', email='user@example.com')
        user.set_password('password')
        user.save()

        res_payload_expected = {
            'detail': 'No active account found with the given credentials'}

        # Act
        res = APIClient().post('/api/token/',
                               {'username': 'WrongUser', 'password': 'abc'},
                               format='json')

        # Assert
        self.assertEqual(res.status_code, 401)
        res_payload_actual = json.loads(res.content)
        self.assertDictEqual(res_payload_actual, res_payload_expected)

    def test_refresh_access_token(self):

        # Arrange
        user = User(username='User', email='user@example.com')
        user.set_password('password')
        user.save()

        api_client = APIClient()
        res_obtain_token = api_client.post('/api/token/', \
            {'username': 'User', 'password': 'password'}, \
            format='json')
        jwt_pair = json.loads(res_obtain_token.content)
        refresh_token = jwt_pair['refresh']

        # Act
        res_refresh = api_client.post('/api/token/refresh/',
                                      {'refresh': refresh_token},
                                      format='json')

        # Assert
        self.assertEqual(res_refresh.status_code, 200)
        new_access_token = json.loads(res_refresh.content)
        self.assertTrue('access' in new_access_token.keys())


class JwtAuthenticationTestCase(TestCase):

    def test_list_clients_with_access_token(self):

        # Arrange
        user = User(username='User', email='user@example.com')
        user.set_password('password')
        user.save()

        client_1 = Client(name='Client 1')
        client_1.save()
        client_2 = Client(name='Client 2')
        client_2.save()
        client_1.assigned_users.set([user])
        client_2.assigned_users.set([user])

        res_payload_expected = [ClientSerializer(client_1).data,
                                ClientSerializer(client_2).data]

        api_client = APIClient()
        res_obtain_jwt = api_client.post('/api/token/', \
            {'username': 'User', 'password': 'password'}, \
            format='json')
        jwt_pair = json.loads(res_obtain_jwt.content)
        access_token = jwt_pair['access']

        # Act
        res_list = api_client.get('/clients/', format='json',
                                  HTTP_AUTHORIZATION=f'Bearer {access_token}')

        # Assert
        self.assertEqual(res_list.status_code, 200)
        res_payload_actual = json.loads(res_list.content)
        self.assertEqual(res_payload_expected, res_payload_actual)

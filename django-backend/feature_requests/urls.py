from django.urls import path

from . import views

urlpatterns = [
    path('', views.FeatureRequestList.as_view()),
    path('<int:pk>', views.FeatureRequestDetail.as_view()),
]

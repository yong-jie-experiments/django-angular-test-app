import json

from django.test import TestCase
from rest_framework.test import APIRequestFactory

from users.models import User
from clients.models import Client
from .models import FeatureRequest
from .views import FeatureRequestList, FeatureRequestDetail
from .serializers import FeatureRequestSerializer

# pylint: disable=line-too-long


class FeatureRequestListBasicCrudTestCase(TestCase):

    def test_list__assigned_user_can_list_feature_requests_of_own_clients(self):

        # Arrange
        user_1 = User(username='User 1', email='user_1@example.com')
        user_1.save()
        user_2 = User(username='User 2', email='user_2@example.com')
        user_2.save()

        client_1 = Client(name='Client 1 of User 1')
        client_1.save()
        client_1.assigned_users.set([user_1])
        client_1.save()
        client_2 = Client(name='Client 2 of User 1')
        client_2.save()
        client_2.assigned_users.set([user_1])
        client_2.save()
        client_3 = Client(name='Client 3')
        client_3.save()
        client_3.assigned_users.set([user_2])
        client_3.save()

        fr_1 = FeatureRequest(title='Feature Request 1', client=client_1)
        fr_1.save()
        fr_2 = FeatureRequest(title='Feature Request 2', client=client_2)
        fr_2.save()
        fr_3 = FeatureRequest(title='Feature Request 3', client=client_3)
        fr_3.save()

        req = APIRequestFactory().get('/feature_requests',
                                      content_type='application/json')
        req.user = user_1
        res_payload_expected = [FeatureRequestSerializer(fr_1).data,
                                FeatureRequestSerializer(fr_2).data]

        # Act
        res = FeatureRequestList.as_view()(req)
        res.render()

        # Assert
        self.assertEqual(res.status_code, 200)
        res_payload_actual = json.loads(res.content)
        self.assertEqual(res_payload_expected, res_payload_actual)

    def test_list__admin_user_can_list_all_feature_requests(self):

        # Arrange
        user_1 = User(username='User 1', email='user_1@example.com')
        user_1.save()
        user_2 = User(username='User 2', email='user_2@example.com')
        user_2.save()
        user_admin = User(username='Admin User',
                          email='admin_user@example.com', is_staff=True)
        user_admin.save()

        client_1 = Client(name='Client 1 of User 1')
        client_1.save()
        client_1.assigned_users.set([user_1])
        client_1.save()
        client_2 = Client(name='Client 2 of User 1')
        client_2.save()
        client_2.assigned_users.set([user_1])
        client_2.save()
        client_3 = Client(name='Client 3')
        client_3.save()
        client_3.assigned_users.set([user_2])
        client_3.save()

        fr_1 = FeatureRequest(title='Feature Request 1', client=client_1)
        fr_1.save()
        fr_2 = FeatureRequest(title='Feature Request 2', client=client_2)
        fr_2.save()
        fr_3 = FeatureRequest(title='Feature Request 3', client=client_3)
        fr_3.save()

        req = APIRequestFactory().get('/feature_requests',
                                      content_type='application/json')
        req.user = user_admin
        res_payload_expected = [FeatureRequestSerializer(fr_1).data,
                                FeatureRequestSerializer(fr_2).data,
                                FeatureRequestSerializer(fr_3).data]

        # Act
        res = FeatureRequestList.as_view()(req)
        res.render()

        # Assert
        self.assertEqual(res.status_code, 200)
        res_payload_actual = json.loads(res.content)
        self.assertEqual(res_payload_expected, res_payload_actual)


class FeatureRequestDetailAuthorizationTestCase(TestCase):

    def test_delete__assigned_user_can_delete_feature_requests_of_own_client(self):

        # Arrange
        user = User(username='User', email='user@example.com')
        user.save()

        client = Client(name='Client')
        client.save()
        client.assigned_users.set([user])
        client.save()

        fr_1 = FeatureRequest(title='Feature Request 1', client=client)
        fr_1.save()
        fr_2 = FeatureRequest(title='Feature Request 2', client=client)
        fr_2.save()

        req_delete = APIRequestFactory().delete('/feature_requests/1',
                                                content_type='application/json')
        req_delete.user = user

        req_list = APIRequestFactory().get('/feature_requests',
                                           content_type='application/json')
        req_list.user = user
        res_list_payload_expected = [FeatureRequestSerializer(fr_1).data]

        # Act
        res_delete = FeatureRequestDetail.as_view()(req_delete, pk=fr_2.pk)
        res_delete.render()

        res_list = FeatureRequestList.as_view()(req_list)
        res_list.render()

        # Assert
        self.assertEqual(res_delete.status_code, 204)
        res_list_payload_actual = json.loads(res_list.content)
        self.assertEqual(res_list_payload_expected, res_list_payload_actual)

    def test_delete__assigned_user_cannot_delete_feature_requests_of_others_client(self):

        # Arrange
        user_1 = User(username='User 1', email='user_1@example.com')
        user_1.save()
        user_2 = User(username='User 2', email='user_2@example.com')
        user_2.save()

        client_1 = Client(name='Client 1')
        client_1.save()
        client_1.assigned_users.set([user_1])
        client_1.save()
        client_2 = Client(name='Client 2')
        client_2.save()
        client_2.assigned_users.set([user_2])
        client_2.save()

        fr_1 = FeatureRequest(title='Feature Request 1', client=client_1)
        fr_1.save()
        fr_2 = FeatureRequest(title='Feature Request 2', client=client_1)
        fr_2.save()
        fr_3 = FeatureRequest(title='Feature Request 3', client=client_2)
        fr_3.save()

        req_delete = APIRequestFactory().delete('/feature_requests/3',
                                                content_type='application/json')
        req_delete.user = user_1

        req_list = APIRequestFactory().get('/feature_requests',
                                           content_type='application/json')
        req_list.user = user_2
        res_list_payload_expected = [FeatureRequestSerializer(fr_3).data]

        # Act
        res_delete = FeatureRequestDetail.as_view()(req_delete, pk=fr_3.pk)
        res_delete.render()

        res_list = FeatureRequestList.as_view()(req_list)
        res_list.render()

        # Assert
        self.assertEqual(res_delete.status_code, 403)
        res_list_payload_actual = json.loads(res_list.content)
        self.assertEqual(res_list_payload_expected, res_list_payload_actual)

    def test_delete__staff_user_can_delete_feature_requests_of_others_client(self):

        # Arrange
        user_non_admin = User(username='Non-Admin User',
                              email='user_1@example.com')
        user_non_admin.save()
        user_admin = User(username='Admin User',
                          email='user_admin@example.com', is_staff=True)
        user_admin.save()

        client = Client(name='Client')
        client.save()
        client.assigned_users.set([user_non_admin])
        client.save()

        fr_1 = FeatureRequest(title='Feature Request 1', client=client)
        fr_1.save()
        fr_2 = FeatureRequest(title='Feature Request 2', client=client)
        fr_2.save()

        req_delete = APIRequestFactory().delete('/feature_requests/2',
                                                content_type='application/json')
        req_delete.user = user_admin

        req_list = APIRequestFactory().get('/feature_requests',
                                           content_type='application/json')
        req_list.user = user_non_admin
        res_list_payload_expected = [FeatureRequestSerializer(fr_1).data]

        # Act
        res_delete = FeatureRequestDetail.as_view()(req_delete, pk=fr_2.pk)
        res_delete.render()

        res_list = FeatureRequestList.as_view()(req_list)
        res_list.render()

        # Assert
        self.assertEqual(res_delete.status_code, 204)
        res_list_payload_actual = json.loads(res_list.content)
        self.assertEqual(res_list_payload_expected, res_list_payload_actual)

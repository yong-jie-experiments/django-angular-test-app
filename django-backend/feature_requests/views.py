from rest_framework import generics

from .models import FeatureRequest
from .serializers import FeatureRequestSerializer
from .permissions import IsAdminUserOrAssignedUser


class FeatureRequestList(generics.ListCreateAPIView):

    permission_classes = []
    serializer_class = FeatureRequestSerializer

    def get_queryset(self):

        user = self.request.user
        if user.is_anonymous:
            return FeatureRequest.objects.none()
        if user.is_staff or user.is_superuser:
            return FeatureRequest.objects.all()
        return FeatureRequest.objects.filter(client__in=user.client_set.all())


class FeatureRequestDetail(generics.RetrieveUpdateDestroyAPIView):

    permission_classes = [IsAdminUserOrAssignedUser]
    queryset = FeatureRequest.objects.all()
    serializer_class = FeatureRequestSerializer

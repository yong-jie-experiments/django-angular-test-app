from django.test import TestCase
from django.urls import resolve

from .views import FeatureRequestDetail, FeatureRequestList


class RoutesTestCase(TestCase):

    def test_all_routes(self):

        self.assertEqual(resolve('/feature_requests/').func.__name__,
                         FeatureRequestList.as_view().__name__)
        self.assertEqual(resolve('/feature_requests/1').func.__name__,
                         FeatureRequestDetail.as_view().__name__)

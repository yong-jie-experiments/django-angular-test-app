from django.contrib import admin

from .models import FeatureRequest


class FeatureRequestAdmin(admin.ModelAdmin):

    fields = ['title', 'description', 'client', 'priority']
    list_display = fields + ['date_created', 'date_modified']
    readonly_fields = ('date_created', 'date_modified')


admin.site.register(FeatureRequest, FeatureRequestAdmin)

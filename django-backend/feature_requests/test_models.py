from django.test import TestCase

from users.models import User
from clients.models import Client
from .models import FeatureRequest

# pylint: disable=line-too-long

class FeatureRequestBasicTestCase(TestCase):

    def test_create(self):

        fr = FeatureRequest(title='Test Feature Request')
        self.assertEqual(fr.title, 'Test Feature Request')

    def test_str(self):

        fr = FeatureRequest(title='Test Feature Request')
        self.assertEqual(str(fr), 'Test Feature Request')

    def test_create_with_no_priority_will_auto_populate_priority(self):

        client = Client(name='Test Client')
        client.save()

        fr1 = FeatureRequest(title='Feature Request 1 with no priority',
                             client=client)
        fr1.save()
        fr2 = FeatureRequest(title='Feature Request 2 with no priority',
                             client=client)
        fr2.save()

        self.assertEqual(fr1.priority, 1)
        self.assertEqual(fr2.priority, 2)

    def test_create_with_too_large_priority_will_auto_populate_with_next_priority(self):

        client = Client(name='Test Client')
        client.save()

        fr1 = FeatureRequest(title='Feature Request 1 with too big priority',
                             client=client, priority=10)
        fr1.save()
        fr2 = FeatureRequest(title='Feature Request 2 with too big priority',
                             client=client, priority=15)
        fr2.save()

        self.assertEqual(fr1.priority, 1)
        self.assertEqual(fr2.priority, 2)

    def test_create_with_in_between_priority_shifts_existing_backwards(self):

        client = Client(name='Test Client')
        client.save()

        fr1 = FeatureRequest(title='Feature Request 1',
                             client=client, priority=1)
        fr1.save()
        fr2 = FeatureRequest(title='Feature Request 2',
                             client=client, priority=2)
        fr2.save()
        fr3 = FeatureRequest(
            title='Feature Request 3 with same priority as existing',
            client=client, priority=2)
        fr3.save()

        fr1.refresh_from_db()
        self.assertEqual(fr1.priority, 1)
        fr2.refresh_from_db()
        self.assertEqual(fr2.priority, 3)
        self.assertEqual(fr3.priority, 2)

    def test_delete_decrement_priority_values_of_lower_priority_feature_requests(self):

        client = Client(name='Test Client')
        client.save()

        fr1 = FeatureRequest(title='Feature Request 1', client=client)
        fr1.save()
        fr2 = FeatureRequest(title='Feature Request 2 to be deleted',
                             client=client)
        fr2.save()
        fr3 = FeatureRequest(title='Feature Request 3', client=client)
        fr3.save()

        fr2.delete()

        fr1.refresh_from_db()
        self.assertEqual(fr1.priority, 1)
        self.assertIsNone(fr2.id, 'Feature Request 2 should have been deleted, '
                          'and have its id set to None.')
        fr3.refresh_from_db()
        self.assertEqual(fr3.priority, 2)

    def test_updating_priority_to_higher_value_within_existing_range_shifts_other_feature_requests_accordingly(self):

        client = Client(name='Test Client')
        client.save()

        fr1 = FeatureRequest(title='Feature Request 1', client=client)
        fr1.save()
        fr2 = FeatureRequest(title='Feature Request 2 to be shifted',
                             client=client)
        fr2.save()
        fr3 = FeatureRequest(title='Feature Request 3', client=client)
        fr3.save()
        fr4 = FeatureRequest(title='Feature Request 4', client=client)
        fr4.save()

        fr2.priority = 3
        fr2.save()

        fr1.refresh_from_db()
        self.assertEqual(fr1.priority, 1)
        fr2.refresh_from_db()
        self.assertEqual(fr2.priority, 3)
        fr3.refresh_from_db()
        self.assertEqual(fr3.priority, 2)
        fr4.refresh_from_db()
        self.assertEqual(fr4.priority, 4)

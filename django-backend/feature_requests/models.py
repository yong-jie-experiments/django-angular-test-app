from django.db import models
from django.db.models import F, Max

from clients.models import Client


class FeatureRequest(models.Model):
    """Model representing a single feature request by a particular client.

    Fields:
        title: Short description of the feature request. Required.
        description: Long description of the feature request. Optional.
        client: Client associated to the feature request. Required.
        priority: Continuous positive integer representing the priority of
            this feature request in respect of the client. Smaller number means
            higher priority. If no priority is provided when creating, the field
            will be automatically populated with the next smallest positive
            integer. If a priority is provided and is of same or higher
            priority than existing feature requests, these existing feature
            requests will be made lower priority than the new feature
            request (i.e., their priority will be incremented by 1).
    """

    title = models.CharField(max_length=128)
    description = models.TextField(blank=True)
    client = models.ForeignKey(to=Client, on_delete=models.CASCADE)
    priority = models.SmallIntegerField()
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):

        return self.title

    def save(self, *args, **kwargs): # pylint: disable=arguments-differ

        if self._is_existing_feature_request(): # SQL UPDATE
            self._update(*args, **kwargs)
        else: # SQL INSERT
            self._insert(*args, **kwargs)

    def delete(self, *args, **kwargs): # pylint: disable=arguments-differ

        super().delete(*args, **kwargs)
        FeatureRequest._decrement_priority_value_starting_from(
            self.priority, self.client)

    def is_client_assigned_to(self, user):

        return self.client.is_assigned_to(user)

    def _insert(self, *args, **kwargs):

        self.priority = FeatureRequest._insert_validate_priority_value(
            self.priority, self.client)
        FeatureRequest._increment_priority_value_starting_from(
            self.priority, self.client)
        super().save(*args, **kwargs)

    def _update(self, *args, **kwargs):

        self.priority = FeatureRequest._update_validate_priority_value(
            self.priority, self.client)

        # Shift priority of other feature requests as appropriate

        final_priority = self.priority
        original_priority = FeatureRequest.objects.get(pk=self.pk).priority

        if original_priority < final_priority:
            # If priority value is increased, decrement priority value for
            # feature requests from original priority+1 to final priority
            FeatureRequest._decrement_priority_value_for_range_inclusive(
                original_priority+1, final_priority, self.client)

        elif original_priority > final_priority:
            # If priority value is decreased, increment priority value for
            # feature requests from final priority to original priority-1
            FeatureRequest._increment_priority_value_for_range_inclusive(
                final_priority, original_priority-1, self.client)

        super().save(*args, **kwargs)

    def _is_existing_feature_request(self):

        return bool(self.id)

    @classmethod
    def _insert_validate_priority_value(cls, priority, client):

        current_max_priority = cls._get_current_max_priority_value(client)
        if not priority:
            return current_max_priority + 1
        if priority > current_max_priority:
            return current_max_priority + 1
        return priority

    @classmethod
    def _update_validate_priority_value(cls, priority, client):

        current_max_priority = cls._get_current_max_priority_value(client)
        if priority < 1:
            return 1
        if priority > current_max_priority:
            return current_max_priority
        return priority

    @classmethod
    def _get_current_max_priority_value(cls, client):

        max_priority_value = cls.objects.filter(client=client)\
            .aggregate(Max('priority')) \
            ['priority__max']

        return max_priority_value or 0

    @classmethod
    def _increment_priority_value_starting_from(cls, start, client):

        cls.objects.filter(client=client, priority__gte=start) \
            .update(priority=F('priority') + 1)

    @classmethod
    def _decrement_priority_value_starting_from(cls, start, client):

        cls.objects.filter(client=client, priority__gte=start) \
            .update(priority=F('priority') - 1)

    @classmethod
    def _increment_priority_value_for_range_inclusive(cls, start, end, client):

        cls.objects.filter(client=client, priority__gte=start,
                           priority__lte=end) \
            .update(priority=F('priority') + 1)

    @classmethod
    def _decrement_priority_value_for_range_inclusive(cls, start, end, client):

        cls.objects.filter(client=client, priority__gte=start,
                           priority__lte=end) \
            .update(priority=F('priority') - 1)

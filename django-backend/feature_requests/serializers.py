from rest_framework import serializers

from .models import FeatureRequest


class FeatureRequestSerializer(serializers.ModelSerializer):

    class Meta:

        model = FeatureRequest
        fields = ['title', 'description', 'client', 'priority', 'date_created',
                  'date_modified']

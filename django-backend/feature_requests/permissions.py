from rest_framework.permissions import BasePermission


class IsAdminUserOrAssignedUser(BasePermission):
    """Permission class for retrieve, update and destroy methods.

    Not to be used for list or create methods.
    """

    def has_permission(self, request, view):

        return True

    def has_object_permission(self, request, view, feature_request): # pylint: disable=arguments-differ

        user = request.user
        if user.is_staff or user.is_superuser:
            return True

        return feature_request.is_client_assigned_to(request.user)

import json

from django.test import TestCase
from django.contrib.auth.models import AnonymousUser
from rest_framework.test import APIRequestFactory

from users.models import User
from .views import ClientDetail, ClientList
from .models import Client
from .serializers import ClientSerializer


class ClientListBasicCrudTestCase(TestCase):

    def test_list(self):

        # Arrange
        client_1 = Client(name='Client 1')
        client_1.save()
        client_2 = Client(name='Client 2')
        client_2.save()
        user = User(username='User', email='user@example.com')
        user.save()
        client_1.assigned_users.set([user])
        client_2.assigned_users.set([user])
        req = APIRequestFactory().get('/clients',
                                      content_type='application/json')
        req.user = user
        res_payload_expected = [ClientSerializer(client_1).data,
                                ClientSerializer(client_2).data]

        # Act
        res = ClientList.as_view()(req)
        res.render()

        # Assert
        self.assertEqual(res.status_code, 200)
        res_payload_actual = json.loads(res.content)
        self.assertEqual(res_payload_expected, res_payload_actual)

    def test_list_with_paging(self):

        pass

    def test_create(self):

        pass

    def test_create_invalid(self):

        pass


class ClientDetailTestCase(TestCase):

    def test_retrieve_one(self):

        # Arrange
        client = Client(name='Client')
        client.save()
        user = User(username='User', email='user@example.com')
        user.save()
        client.assigned_users.set([user])
        req = APIRequestFactory().get('/clients/1',
                                      content_type='application/json')
        req.user = user
        res_payload_expected = ClientSerializer(client).data

        # Act
        res = ClientDetail.as_view()(req, pk=client.pk)
        res.render()

        # Assert
        self.assertEqual(res.status_code, 200)
        res_payload_actual = json.loads(res.content)
        self.assertEqual(res_payload_expected, res_payload_actual)

    def test_retrieve_one_non_existent(self):

        pass

    def test_delete(self):

        # Arrange
        client_1 = Client(name='Client 1')
        client_1.save()
        client_2 = Client(name='Client 2')
        client_2.save()
        user = User(username='User', email='user@example.com', is_staff=True)
        user.save()
        client_1.assigned_users.set([user])
        client_2.assigned_users.set([user])
        req_delete = APIRequestFactory().delete('/clients/2',
                                                content_type='application/json')
        req_delete.user = user
        req_list = APIRequestFactory().get('/clients',
                                           content_type='application/json')
        req_list.user = user
        res_list_payload_expected = [ClientSerializer(client_1).data]

        # Act
        res_delete = ClientDetail.as_view()(req_delete, pk=client_2.pk)
        res_delete.render()

        res_list = ClientList.as_view()(req_list)
        res_list.render()

        # Assert
        self.assertEqual(res_delete.status_code, 204)

        self.assertEqual(res_list.status_code, 200)
        res_list_payload_actual = json.loads(res_list.content)
        self.assertEqual(res_list_payload_expected, res_list_payload_actual)

    def test_delete_non_existent(self):

        pass

    def test_update(self):

        pass

    def test_update_non_existent(self):

        pass


class ClientListAuthorizationTestCase(TestCase):

    def test_user_can_only_see_own_clients(self):

        # Arrange
        client_1 = Client(name='Test Client 1')
        client_1.save()
        client_2 = Client(name='Test Client 2')
        client_2.save()

        user_1 = User(username='User 1', email='user_1@example.com')
        user_1.save()
        user_2 = User(username='User 2', email='user_2@example.com')
        user_2.save()

        client_1.assigned_users.set([user_1])
        client_1.save()
        client_2.assigned_users.set([user_1, user_2])
        client_2.save()

        request_factory = APIRequestFactory()
        req_1 = request_factory.get('/clients', content_type='application/json')
        req_1.user = user_1
        req_2 = request_factory.get('/clients', content_type='application/json')
        req_2.user = user_2

        res_1_payload_expected = [ClientSerializer(client_1).data,
                                  ClientSerializer(client_2).data]
        res_2_payload_expected = [ClientSerializer(client_2).data]

        # Act
        res_1 = ClientList.as_view()(req_1)
        res_1.render()
        res_2 = ClientList.as_view()(req_2)
        res_2.render()

        # Assert
        self.assertEqual(res_1.status_code, 200)
        res_1_payload_actual = json.loads(res_1.content)
        self.assertEqual(res_1_payload_expected, res_1_payload_actual)

        self.assertEqual(res_2.status_code, 200)
        res_2_payload_actual = json.loads(res_2.content)
        self.assertEqual(res_2_payload_expected, res_2_payload_actual)

    def test_anonymous_user_cannot_see_clients(self):

        # Arrange
        client_1 = Client(name='Test Client 1')
        client_1.save()
        client_2 = Client(name='Test Client 2')
        client_2.save()

        user = AnonymousUser()

        req = APIRequestFactory().get('/clients',
                                      content_type='application/json')
        req.user = user

        res_payload_expected = []

        # Act
        res = ClientList.as_view()(req)
        res.render()

        # Assert
        self.assertEqual(res.status_code, 200)
        res_payload_actual = json.loads(res.content)
        self.assertEqual(res_payload_expected, res_payload_actual)


class ClientDetailAuthorizationTestCase(TestCase):

    def test_non_staff_user_cannot_delete_own_client(self):

        # Arrange
        client = Client(name='Test Client')
        client.save()

        user = User(username='User', email='user@example.com', is_staff=False)
        user.save()

        client.assigned_users.set([user])
        client.save()

        req = APIRequestFactory().delete('/clients',
                                         content_type='application/json')
        req.user = user
        res_payload_expected = {
            'detail': 'You do not have permission to perform this action.'}

        # Act
        res = ClientDetail.as_view()(req, pk=client.pk)
        res.render()

        # Assert
        self.assertEqual(res.status_code, 403)
        res_payload_actual = json.loads(res.content)
        self.assertEqual(res_payload_expected, res_payload_actual)

    def test_staff_user_can_delete_others_client(self):

        # Arrange
        client = Client(name='Test Client')
        client.save()

        user_staff = User(username='User (Staff)',
                          email='user_staff@example.com', is_staff=True)
        user_staff.save()
        user_non_staff = User(username='User',
                              email='user@example.com', is_staff=False)
        user_non_staff.save()

        client.assigned_users.set([user_non_staff])
        client.save()

        request_factory = APIRequestFactory()
        req_delete = request_factory.delete('/clients',
                                            content_type='application/json')
        req_delete.user = user_staff
        req_list = request_factory.get('/clients',
                                       content_type='application/json')
        req_list.user = user_non_staff

        res_list_payload_expected = []

        # Act
        res_delete = ClientDetail.as_view()(req_delete, pk=client.pk)
        res_delete.render()
        res_list = ClientList.as_view()(req_list)
        res_list.render()

        # Assert
        self.assertEqual(res_delete.status_code, 204, res_delete.content)
        res_list_payload_actual = json.loads(res_list.content)
        self.assertEqual(res_list_payload_expected, res_list_payload_actual)

# Generated by Django 2.2.5 on 2019-09-04 16:04

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('clients', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='assigned_users',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
    ]

from rest_framework import generics

from .models import Client
from .serializers import ClientSerializer
from .permissions import IsAdminUserOrAssignedUser


class ClientList(generics.ListCreateAPIView):

    permission_classes = []
    serializer_class = ClientSerializer

    def get_queryset(self):

        user = self.request.user
        if user.is_anonymous:
            return Client.objects.none()
        if user.is_staff or user.is_superuser:
            return Client.objects.all()
        return user.client_set


class ClientDetail(generics.RetrieveUpdateDestroyAPIView):

    permission_classes = [IsAdminUserOrAssignedUser]
    queryset = Client.objects.all()
    serializer_class = ClientSerializer

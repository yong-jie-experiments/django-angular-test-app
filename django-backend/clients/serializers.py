from rest_framework import serializers

from .models import Client


class ClientSerializer(serializers.ModelSerializer):

    class Meta:

        model = Client
        fields = ['name', 'date_created', 'date_modified']

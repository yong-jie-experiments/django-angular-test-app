from django.test import TestCase
from django.urls import resolve

from .views import ClientDetail, ClientList


class RoutesTestCase(TestCase):

    def test_all_routes(self):

        self.assertEqual(resolve('/clients/').func.__name__,
                         ClientList.as_view().__name__)
        self.assertEqual(resolve('/clients/1').func.__name__,
                         ClientDetail.as_view().__name__)

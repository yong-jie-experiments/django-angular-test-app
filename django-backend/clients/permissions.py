from rest_framework.permissions import BasePermission, SAFE_METHODS


class IsAdminUserOrAssignedUser(BasePermission):
    """Permission class for retrieve, update and destroy methods.

    Not to be used for list or create methods.
    """

    def has_permission(self, request, view):

        return True

    def has_object_permission(self, request, view, obj):

        user = request.user
        if user.is_staff or user.is_superuser:
            return True

        return request.method in SAFE_METHODS \
            and obj.is_assigned_to(request.user)

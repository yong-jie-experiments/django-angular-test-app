from django.test import TestCase

from users.models import User
from .models import Client


class ClientBasicTestCase(TestCase):

    def test_create(self):

        client = Client(name='Test Client')
        self.assertEqual(client.name, 'Test Client')

    def test_str(self):

        client = Client(name='Test Client Name')
        self.assertEqual(str(client), 'Test Client Name')


class ClientAssignedUsersTestCase(TestCase):

    def test_set_assigned_users(self):

        client = Client(name='Another Test Client')
        client.save()

        user_1 = User(username='User 1', email='user_1@example.com')
        user_1.save()
        user_2 = User(username='User 2', email='user_2@example.com')
        user_2.save()

        client.assigned_users.add(user_1)
        client.assigned_users.add(user_2)
        client.save()

        self.assertEqual(client.name, 'Another Test Client')
        self.assertEqual(client.assigned_users.count(), 2)
        self.assertEqual(list(client.assigned_users.all()), [user_1, user_2])

    def test_remove_some_assigned_users(self):

        client = Client(name='Yet Another Test Client')
        client.save()

        user_a = User(username='User A', email='user_a@example.com')
        user_a.save()
        user_b = User(username='User B', email='user_b@example.com')
        user_b.save()

        client.assigned_users.set([user_a, user_b])
        client.save()
        client.assigned_users.remove(user_a)

        self.assertEqual(client.name, 'Yet Another Test Client')
        self.assertEqual(client.assigned_users.count(), 1)
        self.assertEqual(list(client.assigned_users.all()), [user_b])

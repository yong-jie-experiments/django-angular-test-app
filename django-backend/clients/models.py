from django.db import models

from users.models import User


class Client(models.Model):

    name = models.CharField(max_length=128)
    assigned_users = models.ManyToManyField(User)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):

        return self.name

    def is_assigned_to(self, user):

        return user in self.assigned_users.all()

from django.contrib import admin

from .models import Client


def assigned_users_joined(obj):

    usernames = [u.username for u in obj.assigned_users.all()]
    return ', '.join(usernames)

assigned_users_joined.short_description = 'Assigned Users'


class ClientAdmin(admin.ModelAdmin):

    fields = ['name', 'assigned_users']
    list_display = ['name', assigned_users_joined, 'date_created',
                    'date_modified']
    readonly_fields = ('date_created', 'date_modified')


admin.site.register(Client, ClientAdmin)

from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    """Custom user class as recommended by Django official documentation

    Link to official documentation:
    https://docs.djangoproject.com/en/2.2/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project
    """

    pass # pylint: disable=unnecessary-pass

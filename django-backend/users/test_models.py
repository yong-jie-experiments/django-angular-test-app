from django.test import TestCase

from .models import User


class UserTestCase(TestCase):
    """Example test class with `User` as the subject-under-test.

    Extensive testin of `User` is currently not needed as it is a bare
    subclass of Django`s `AbstractUser`
    """

    def test_create(self):

        user = User(username='Test User', email='test_user@example.com')
        self.assertEqual(user.username, 'Test User')
        self.assertEqual(user.email, 'test_user@example.com')

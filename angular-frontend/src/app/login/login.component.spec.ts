import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { of } from 'rxjs';

import { LoginComponent } from './login.component';
import { LoginService } from './login.service';
import { Credential } from './credential.interface';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async(() => {
    const spy = jasmine.createSpyObj('LoginService', ['login']);
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      providers: [ {provide: LoginService, useValue: spy} ],
      imports: [
        BrowserAnimationsModule,
        MatButtonModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule,
       ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call login() on LoginService when submitted', () => {
    // Arrange
    const loginServiceSpy = TestBed.get(LoginService);
    const placeholderReturnValue = of([]);
    loginServiceSpy.login.and.returnValue(placeholderReturnValue);
    const credential: Credential = {
      username: 'user@example.com', password: 'thePassword'};

    // Act
    component.loginForm.setValue(credential);
    component.onSubmit();

    // Assert
    expect(loginServiceSpy.login).toHaveBeenCalledWith(credential);
  });

  it('should display username validation error as appropriate', () => {
    // Arrange
    const usernameInput =
      fixture.debugElement.query(By.css('input[formcontrolname="username"]'));
    const eventObject = null;
    expect(fixture.debugElement.query(By.css('.mat-error'))).toBeNull();

    // Act
    usernameInput.triggerEventHandler('focus', eventObject);
    fixture.detectChanges();
    usernameInput.triggerEventHandler('blur', eventObject);
    fixture.detectChanges();

    // Assert
    expect(fixture.debugElement.query(By.css('.mat-error')).nativeElement
        .textContent)
      .toContain('Username is required.');
  });

  it('should display password validation error as appropriate', () => {
    // Arrange
    const passwordInput =
      fixture.debugElement.query(By.css('input[formcontrolname="password"]'));
    const eventObject = null;
    expect(fixture.debugElement.query(By.css('.mat-error'))).toBeNull();

    // Act
    passwordInput.triggerEventHandler('focus', eventObject);
    fixture.detectChanges();
    passwordInput.triggerEventHandler('blur', eventObject);
    fixture.detectChanges();

    // Assert
    expect(fixture.debugElement.query(By.css('.mat-error')).nativeElement
        .textContent)
      .toContain('Password is required.');
  });
});

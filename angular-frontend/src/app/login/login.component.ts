import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

import { LoginError } from './login-error.interface';
import { LoginService } from './login.service';
import { Credential } from './credential.interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  loginForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['', [Validators.required, Validators.minLength(8)]],
  });
  errorDetails = '';
  usernameErrorFromServer = '';
  passwordErrorFromServer = '';

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService
  ) { }

  printHello() {
    console.log('hello');
  }
  clearErrorMsg() {
    this.errorDetails = '';
    this.usernameErrorFromServer = '';
    this.passwordErrorFromServer = '';
  }

  displayErrorMsg(errorMsg: LoginError) {
    this.errorDetails = errorMsg.detail;
    this.usernameErrorFromServer = errorMsg.username;
    this.passwordErrorFromServer = errorMsg.password;
  }

  getErrorMsg(formControlName: string) {
    const formControl = this.loginForm.controls[formControlName];
    if (formControl.status !== 'INVALID') {
      return;
    }

    const errorMsgs: string[] = [];
    switch (formControlName) {
      case 'username': {
        if ('required' in formControl.errors) {
          errorMsgs.push('Username is required.');
        }
        if (this.usernameErrorFromServer) {
          errorMsgs.push(this.usernameErrorFromServer);
        }
        break;
      }
      case 'password': {
        if ('required' in formControl.errors) {
          errorMsgs.push('Password is required.');
        }
        if ('minlength' in formControl.errors) {
          errorMsgs.push('Min 8 characters.');
        }
        if (this.passwordErrorFromServer) {
          errorMsgs.push(this.passwordErrorFromServer);
        }
        break;
      }
    }
    return errorMsgs.join(' ');
  }

  onSubmit() {
    this.clearErrorMsg();
    if (!this.loginForm.valid) {
      return;
    }

    const credential: Credential = {
      username: this.loginForm.value.username,
      password: this.loginForm.value.password,
    };
    this.loginService.login(credential).subscribe({
        error: (err: LoginError) => this.displayErrorMsg(err),
    });
  }
}

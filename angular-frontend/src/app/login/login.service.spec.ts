import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { of, throwError, Observable } from 'rxjs';

import { Credential } from './credential.interface';
import { LoginError } from './login-error.interface';
import { LoginService } from './login.service';

describe('LoginService', () => {
  beforeEach(() => {
    const spy = jasmine.createSpyObj('HttpClient', ['post']);
    TestBed.configureTestingModule({
      providers: [
        LoginService,
        {provide: HttpClient, useValue: spy},
      ],
      imports: [],
    })
    .compileComponents();
  });

  it('should be created', () => {
    const service: LoginService = TestBed.get(LoginService);
    expect(service).toBeTruthy();
  });

  it('should call post() on HttpClient when logging in', () => {
    // Arrange
    const service: LoginService = TestBed.get(LoginService);
    const httpClientSpy = TestBed.get(HttpClient);
    httpClientSpy.post.and.returnValue(of([]));

    const credential: Credential = {username: 'username', password: 'password'};
    const loginEndpoint = 'http://localhost:8000/api/token/';

    // Act
    service.login(credential);

    // Assert
    expect(httpClientSpy.post).toHaveBeenCalledWith(loginEndpoint, credential);
  });

  it('should save the JwtToken to localstorage if no error is returned from server', (done: DoneFn) => {
    // Arrange
    const localStorageSetItem = spyOn(localStorage, 'setItem').and.stub();
    const service: LoginService = TestBed.get(LoginService);
    const httpClientSpy = TestBed.get(HttpClient);
    const stubJwtToken = {
      access: 'the_access_token', refresh: 'the_refresh_token'};
    httpClientSpy.post.and.returnValue(of(stubJwtToken));
    const credential: Credential = {username: 'username', password: 'password'};

    // Act
    const httpResponse = service.login(credential);

    // Assert
    expect(httpClientSpy.post).toHaveBeenCalled();
    httpResponse.subscribe({
      next: () => {
        expect(localStorageSetItem).toHaveBeenCalledWith(
          'access_token', 'the_access_token');
        expect(localStorageSetItem).toHaveBeenCalledWith(
          'refresh_token', 'the_refresh_token');
        expect(localStorageSetItem).toHaveBeenCalledWith(
          'current_user', 'username');
        done();
      },
    });
  });

  it('should convert and rethrow friendly error if there is error sending request to server', (done: DoneFn) => {
    // Arrange
    const service: LoginService = TestBed.get(LoginService);
    const httpClientSpy = TestBed.get(HttpClient);
    // When HttpClient.post() fails because of client-side / network error, it
    // returns a HttpErrorResponse with an EventError as a property called
    // "error".
    const stubError = new HttpErrorResponse({
      error: new ErrorEvent('Stub ErrorEvent', {message: 'Stub error message.'})
    });
    httpClientSpy.post.and.returnValue(throwError(stubError));

    const credential: Credential = {username: 'username', password: 'password'};
    const expectedErrorMsg =
      'An unexpected error has occurred. '
      + 'Please check your internet connection or try again later.';
    const expectedFriendlyError: LoginError = {detail: expectedErrorMsg};

    // Act
    const observable = service.login(credential);

    // Assert
    expect(httpClientSpy.post).toHaveBeenCalled();
    observable.subscribe({
      error: (error: LoginError) => {
        expect(error).toEqual(expectedFriendlyError);
        done();
      },
    });
  });

  it('should convert and rethrow friendly error if there is error returned from server', (done: DoneFn) => {
    // Arrange
    const service: LoginService = TestBed.get(LoginService);
    const httpClientSpy = TestBed.get(HttpClient);
    // When HttpClient.post() fails because of error response code from server, it
    // returns a HttpErrorResponse WITHOUT an EventError as a property called
    // "error".
    const stubError = new HttpErrorResponse({
      error: {unkwnowErrorFormat: 'Unknown Error Format'},
      status: 401,
    });
    httpClientSpy.post.and.returnValue(throwError(stubError));

    const credential: Credential = {username: 'username', password: 'password'};
    const friendlyErrorMsg =
      'An unexpected error has occurred. Please try again later.';
    const expectedFriendlyError: LoginError = {detail: friendlyErrorMsg};

    // Act
    const httpResponse: Observable<void> = service.login(credential);

    // Assert
    expect(httpClientSpy.post).toHaveBeenCalled();
    httpResponse.subscribe({
      error: (error: LoginError) => {
        expect(error).toEqual(expectedFriendlyError);
        done();
      },
    });
  });

  it('should forward the error if the error is in the expected form of LoginError', (done: DoneFn) => {
    // Arrange
    const service: LoginService = TestBed.get(LoginService);
    const httpClientSpy = TestBed.get(HttpClient);
    // When HttpClient.post() fails because of error response code from server, it
    // returns a HttpErrorResponse WITHOUT an EventError as a property called
    // "error".
    const stubError = new HttpErrorResponse({
      error: {detail: 'No active account found with the given credentials'},
      status: 401,
    });
    httpClientSpy.post.and.returnValue(throwError(stubError));

    const credential: Credential = {username: 'username', password: 'password'};
    const friendlyErrorMsg =
      'No active account found with the given credentials';
    const expectedFriendlyError: LoginError = {detail: friendlyErrorMsg};

    // Act
    const httpResponse: Observable<void> = service.login(credential);

    // Assert
    expect(httpClientSpy.post).toHaveBeenCalled();
    httpResponse.subscribe({
      error: (error: LoginError) => {
        expect(error).toEqual(expectedFriendlyError);
        done();
      },
    });
  });
});

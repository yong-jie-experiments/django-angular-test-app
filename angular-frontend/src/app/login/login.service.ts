import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { catchError, first, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import { Credential } from './credential.interface';
import { isLoginError, LoginError } from './login-error.interface';
import { JwtToken } from './jwt-token.interface';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  loginEndpoint = 'http://localhost:8000/api/token/';

  constructor(private http: HttpClient) { }

  login(credential: Credential): Observable<void> {
    return this.http.post<JwtToken>(
        this.loginEndpoint, credential)
      .pipe(
        first(),
        map((jwtToken: JwtToken) => {
          this.setJwtToken(jwtToken);
          this.setCurrentUser(credential.username);
        }),
        catchError(this.handleError),
        );
  }

  logout() {
    this.unsetJwtToken();
    this.unsetCurrentUser();
  }

  setJwtToken(jwtToken: JwtToken) {
    localStorage.setItem('access_token', jwtToken.access);
    localStorage.setItem('refresh_token', jwtToken.refresh);
  }

  unsetJwtToken() {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
  }

  setCurrentUser(username: string) {
    localStorage.setItem('current_user', username);
  }

  unsetCurrentUser() {
    localStorage.removeItem('current_user');
  }

  private handleError(error: HttpErrorResponse): Observable<void> {
    if (isLoginError(error.error)) {
      // An expected login error
      return throwError(error.error);
    }

    // An unexpected error
    console.error('An unexpected error has occurred:');
    console.error(error.error);
    const friendlyErrorMsg: LoginError = {
      detail: 'An unexpected error has occurred.'};
    if (error.error instanceof ErrorEvent) {
      // An unexpected client-side or network error occurred.
      friendlyErrorMsg.detail +=
        ' Please check your internet connection or try again later.';
    } else {
      // An enexpected unsuccessful response code from server.
      friendlyErrorMsg.detail += ' Please try again later.';
    }
    return throwError(friendlyErrorMsg);
  }
}

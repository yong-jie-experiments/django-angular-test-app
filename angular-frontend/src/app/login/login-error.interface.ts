export interface LoginError {
  username?: string;
  password?: string;
  detail?: string;
}

export function isLoginError(obj: any): obj is LoginError {
  return 'password' in obj || 'username' in obj || 'detail' in obj;
}
